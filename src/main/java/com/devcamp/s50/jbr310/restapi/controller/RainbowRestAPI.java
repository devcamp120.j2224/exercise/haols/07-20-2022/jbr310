package com.devcamp.s50.jbr310.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class RainbowRestAPI {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> getListRainbowColor() {
        ArrayList<String> ColorRainBow = new ArrayList<String>();
        ColorRainBow.add("Red");
        ColorRainBow.add("Orange");
        ColorRainBow.add("Yellow");
        ColorRainBow.add("Green");
        ColorRainBow.add("Blue");
        ColorRainBow.add("Indigo");
        ColorRainBow.add("Violet");
        return ColorRainBow;

    }

}
